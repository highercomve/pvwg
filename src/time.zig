const std = @import("std");

const secs_per_min: u64 = 60;
const secs_per_hour: u64 = 60 * 60;
const secs_per_day: u64 = 24 * 60 * 60;
const days_per_year: u64 = 365;
const days_per_leap_year: u64 = 366;

fn formatTimestampToUTC(allocator: std.mem.Allocator, timestamp_ns: u64) ![]const u8 {
    // Seconds and nanoseconds since epoch
    const seconds_since_epoch = @divTrunc(timestamp_ns, 1_000_000_000);
    // const nanoseconds = @rem(timestamp_ns, 1_000_000_000);

    var seconds = seconds_since_epoch;
    var year: u64 = 1970;
    var month: u64 = 1;
    var day: u64 = 1;
    var hour: u64 = 0;
    var minute: u64 = 0;
    var second: u64 = 0;

    // Calculate year
    while (seconds >= (secs_per_day * days_per_year)) : (year += 1) {
        if (isLeapYear(year)) {
            if (seconds < secs_per_day * days_per_leap_year) break;
            seconds -= secs_per_day * days_per_leap_year;
        } else {
            seconds -= secs_per_day * days_per_year;
        }
    }

    // Calculate month and day
    const days_in_month: [12]u64 = [12]u64{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    const days_in_leap_month: [12]u64 = [12]u64{ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    const month_days = if (isLeapYear(year)) days_in_leap_month else days_in_month;
    while (seconds >= secs_per_day * month_days[month - 1]) : (month += 1) {
        seconds -= secs_per_day * month_days[month - 1];
    }

    // Calculate day
    day += seconds / secs_per_day;
    seconds %= secs_per_day;

    // Calculate hour
    hour = @divTrunc(seconds, secs_per_hour);
    seconds = @rem(seconds, secs_per_hour);

    // Calculate minute
    minute = @divTrunc(seconds, secs_per_min);
    second = @rem(seconds, secs_per_min);

    // Format the time into a string
    const utc = try std.fmt.allocPrint(allocator, "{d}-{d}-{d}T{d}:{d}:{d}Z", .{
        year,
        month,
        day,
        hour,
        minute,
        second,
    });

    return utc;
}

fn isLeapYear(year: u64) bool {
    return (@rem(year, 4) == 0 and @rem(year, 100) != 0) or (@rem(year, 400) == 0);
}
