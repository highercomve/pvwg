const std = @import("std");

const WATCHDOG_DEVICE = "/dev/watchdog";
const WDIOC_GETTIMEOUT = 0x80045707;
const WDIOC_KEEPALIVE = 0x80045705;

const Error = error{
    FailedIOCtlCall,
    FailedToOpenDevice,
    FailedToGetTimeout,
    FailedToPingWatchdog,
};

fn readMeta(allocator: std.mem.Allocator, t: []const u8, key: []const u8) ![]u8 {
    const filename = std.fmt.allocPrint(
        allocator,
        "/pantavisor/{s}-meta/pvwg.{s}",
        .{ t, key },
    ) catch unreachable;
    defer allocator.free(filename);

    try std.fs.cwd().access(filename, .{ .mode = .read_only });

    const file = try std.fs.cwd().openFile(filename, .{});
    defer file.close();

    const content = try file.readToEndAlloc(allocator, std.math.maxInt(usize));

    return content;
}

fn readUserMeta(allocator: std.mem.Allocator, key: []const u8) ![]u8 {
    return readMeta(allocator, "user", key);
}

fn ioctl(fd: i32, request: u32, arg: *anyopaque) !usize {
    const res = std.os.linux.ioctl(fd, request, @as(usize, @intFromPtr(arg)));
    if (res < 0) {
        return Error.FailedIOCtlCall;
    }
    return res;
}

fn get_watchdog_timeout(fd: i32) i32 {
    var timeout: i32 = 10;
    _ = try ioctl(fd, WDIOC_GETTIMEOUT, &timeout);
    std.log.debug("watchdog timeout: {d}", .{timeout});
    return timeout;
}

fn ping_watchdog(fd: i32) !void {
    var arg: i32 = 0;
    _ = ioctl(fd, WDIOC_KEEPALIVE, &arg) catch return Error.FailedToPingWatchdog;
}

fn is_disabled(allocator: std.mem.Allocator) bool {
    const disabled = readUserMeta(allocator, "disabled") catch {
        return false;
    };
    return std.mem.eql(u8, disabled, "true");
}

fn get_ping_interval(allocator: std.mem.Allocator, default: i32) i32 {
    const pi = readUserMeta(allocator, "ping-interval") catch {
        return default;
    };

    const ping_interval = std.fmt.parseInt(i32, pi, 10) catch {
        return default;
    };

    return ping_interval;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const allocator = arena.allocator();
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile(
        WATCHDOG_DEVICE,
        .{ .mode = .read_write },
    );
    defer file.close();

    const timeout = get_watchdog_timeout(file.handle);
    const default_ping_interval = @divTrunc(timeout, 2);

    var disabled = is_disabled(allocator);
    var ping_interval = get_ping_interval(allocator, default_ping_interval);

    const debug_env = std.process.getEnvVarOwned(allocator, "DEBUG") catch "false";
    defer allocator.free(debug_env);

    const is_debug = std.mem.eql(u8, debug_env, "true");
    var print_messages = true;

    try stdout.print(
        "Watchdog pinger started. Timeout is {d} seconds.\nPinging every {d} seconds. debug mode {s}\n",
        .{ timeout, ping_interval, debug_env },
    );

    while (true) {
        disabled = is_disabled(allocator);
        ping_interval = get_ping_interval(
            allocator,
            default_ping_interval,
        );
        if (disabled) {
            try stdout.print("Watchdog pinger disabled.\n", .{});
            continue;
        }

        ping_watchdog(file.handle) catch |err| {
            try stdout.print("Error pinging watchdog: {s}\n", .{err});
        };

        if (print_messages) {
            try stdout.print("Pinged watchdog at time: {d}\n", .{std.time.timestamp()});
        } else {
            std.log.debug("Pinged watchdog at time: {d}", .{std.time.timestamp()});
        }
        if (!is_debug and print_messages) {
            print_messages = false;
        }

        if (is_debug) {
            std.log.debug("Arena allocator memory usage: {}", .{arena.state.end_index});
        }

        std.time.sleep(@as(u64, @intCast(ping_interval)) * std.time.ns_per_s);
    }
}
