FROM --platform=$BUILDPLATFORM alpine:3.18 as zig

ARG TARGETPLATFORM
ARG BUILDPLATFORM

RUN apk update && apk add --no-cache build-base curl git tar xz

WORKDIR /tmp
RUN curl -L https://ziglang.org/download/0.13.0/zig-linux-x86_64-0.13.0.tar.xz | tar xJf - -C ./
RUN mv zig-linux-x86_64-0.13.0/zig /usr/bin && \
  mv zig-linux-x86_64-0.13.0/lib/* /usr/lib && \
  rm -rf zig-linux-x86_64-0.13.0

FROM --platform=$BUILDPLATFORM zig as builder

ARG TARGETPLATFORM
ARG BUILDPLATFORM

WORKDIR /app
COPY . .

RUN ./build.sh $TARGETPLATFORM

FROM scratch

COPY --from=builder /app/zig-out/bin/pvwg /usr/bin/pvwg

ENTRYPOINT ["/usr/bin/pvwg"]
